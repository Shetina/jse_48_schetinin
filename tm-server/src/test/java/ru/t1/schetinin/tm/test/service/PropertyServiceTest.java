package ru.t1.schetinin.tm.test.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.service.PropertyService;

@Category(UnitCategory.class)
public class PropertyServiceTest {

    @NotNull
    public static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @Test
    public void testGetAuthorProperty() {
        Assert.assertNotNull(PROPERTY_SERVICE.getAuthorEmail());
        Assert.assertNotNull(PROPERTY_SERVICE.getAuthorName());
    }

    @Test
    public void testGetApplicationProperty() {
        Assert.assertNotNull(PROPERTY_SERVICE.getApplicationVersion());
        Assert.assertNotNull(PROPERTY_SERVICE.getApplicationName());
        Assert.assertNotNull(PROPERTY_SERVICE.getApplicationConfig());
        Assert.assertNotNull(PROPERTY_SERVICE.getApplicationLog());
        Assert.assertNotNull(PROPERTY_SERVICE.getAuthorName());
    }

    @Test
    public void testGetGITProperty() {
        Assert.assertNotNull(PROPERTY_SERVICE.getGitCommitId());
        Assert.assertNotNull(PROPERTY_SERVICE.getGitBranch());
        Assert.assertNotNull(PROPERTY_SERVICE.getGitCommitterName());
        Assert.assertNotNull(PROPERTY_SERVICE.getGitCommitterEmail());
        Assert.assertNotNull(PROPERTY_SERVICE.getGitCommitMessage());
        Assert.assertNotNull(PROPERTY_SERVICE.getGitCommitTime());
    }

    @Test
    public void testGetServerProperty() {
        Assert.assertNotNull(PROPERTY_SERVICE.getServerPort());
        Assert.assertNotNull(PROPERTY_SERVICE.getServerHost());
        Assert.assertNotNull(PROPERTY_SERVICE.getSessionTimeout());
        Assert.assertNotNull(PROPERTY_SERVICE.getSessionKey());
    }

}
